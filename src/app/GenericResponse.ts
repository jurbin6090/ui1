export class GenericResponse {
  id: number;
  message: string;
}
