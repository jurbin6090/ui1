import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GenericResponse} from '../GenericResponse';

@Injectable({
  providedIn: 'root'
})
export class Api1Service {

  constructor(private httpClient: HttpClient) {
  }

  callHome(baseUrl: string): Observable<string> {
    return this.httpClient.get(baseUrl + '/', {responseType: 'text'});
  }

  callApi1(baseUrl: string): Observable<string> {
    return this.httpClient.get(baseUrl + '/api1', {responseType: 'text'});
  }

  callRemoteDemo(baseUrl: string): Observable<string> {
    return this.httpClient.get(baseUrl + '/remote', {responseType: 'text'});
  }

  callRemote2Demo(baseUrl: string, destUrl: string): Observable<GenericResponse> {
    return this.httpClient.post<GenericResponse>(baseUrl + '/remote2?uri=' + destUrl, undefined);
  }
}
