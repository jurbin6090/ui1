import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Api2Service {

  constructor(private httpClient: HttpClient) {
  }

  callHome(baseUrl: string): Observable<string> {
    return this.httpClient.get(baseUrl + '/', {responseType: 'text'});
  }

  callApi2(baseUrl: string): Observable<string> {
    return this.httpClient.get(baseUrl + '/api2', {responseType: 'text'});
  }

  callTest(baseUrl: string, destUrl: string): Observable<string> {
    return this.httpClient.get(baseUrl + '/test?uri=' + destUrl, {responseType: 'text'});
  }
}
