import {Component} from '@angular/core';
import {Api1Service} from './service/api1.service';
import {Api2Service} from './service/api2.service';
import {GenericResponse} from './GenericResponse';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  home = '';
  api1 = '';
  remoteDemo = '';
  remoteDemo2 = '';
  home2 = '';
  api2 = '';
  test = '';
  selectInput = 'http://api1-ats-test-3.apps.dev-kubernetes.os.imcc.com';
  input = '';
  selectInput2 = 'http://api2-ats-test-3.apps.dev-kubernetes.os.imcc.com';
  input2 = '';
  constructor(private api1Service: Api1Service, private api2Service: Api2Service) {
  }

  setInput(target) {
    this.selectInput = target.value;
  }

  setInput2(target) {
    this.selectInput2 = target.value;
  }

  callHome() {
    this.api1Service.callHome(this.selectInput).subscribe((response: string) => {
      this.home = response;
    });
  }

  callApi1() {
    this.api1Service.callApi1(this.selectInput).subscribe((response: string) => {
      this.api1 = response;
    });
  }

  callRemoteDemo() {
    this.api1Service.callRemoteDemo(this.selectInput).subscribe((response: string) => {
      this.remoteDemo = response;
    });
  }

  callRemote2Demo() {
    this.api1Service.callRemote2Demo(this.selectInput, this.input2).subscribe((response: GenericResponse) => {
      this.remoteDemo2 = response.id + ': ' + response.message;
    });
  }

  callHome2() {
    this.api2Service.callHome(this.selectInput2).subscribe((response: string) => {
      this.home2 = response;
    });
  }

  callApi2() {
    this.api2Service.callApi2(this.selectInput2).subscribe((response: string) => {
      this.api2 = response;
    });
  }

  callTest() {
    this.api2Service.callTest(this.selectInput2, this.input).subscribe((response: string) => {
      this.test = response;
    });
  }
}
